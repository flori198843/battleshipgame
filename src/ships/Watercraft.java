package ships;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.locks.ReentrantLock;

public class Watercraft {
	
	protected enum ShipType {AIRCRAFTCARRIER, BATTLESHIP, CRUISER, DESTROYER} 
	
	protected ShipType shipType;
	protected final int cntRows = 13;
	protected final int cntColumns = 12;
	protected String name = ""; 
	
	protected int maxHits = 0;
	protected int positionsOnGamefield[][] = new int[cntRows][cntColumns];
	
	public static ArrayList <Watercraft> shipsPlayer = new ArrayList<Watercraft>();
	public static ArrayList <Watercraft> shipsNPC = new ArrayList<Watercraft>();
		
	public Watercraft(ShipType shipType) {
				
		switch(shipType) {
		case AIRCRAFTCARRIER:
			maxHits = 5;
			name = "Aircraftcarrier";
			break;
		case BATTLESHIP:
			maxHits = 4;
			name = "Battleship";
			break;
		case CRUISER:
			maxHits = 3;
			name = "Cruiser";
			break;
		case DESTROYER:
			maxHits = 2;
			name = "Destroyer";
			break;		
		}
	}

	/** 	
	 * @param shipList
	 * @param ship
	 * this method removes a ship out of a specific list of type ship
	 */
	
	public void setDestroyed(Iterator <Watercraft> iterShipList) {
					
			iterShipList.remove();
		}
	
	/**
	 * @param cntShips
	 * puts the cntShips x thisShipType into both Watercraft lists
	 */
	
	public void addShipToLists(boolean player) {		
		if (!player) {
			shipsNPC.add(this);
		} else {
			shipsPlayer.add(this);
		}							
	}	
	
	/* Getter AND Setter */
	
	public int getMaxHits() {
		return maxHits; 		
	}
	
	public void decMaxHits() {
		maxHits--; 		
	}
	
	public ShipType getShipType() {
		return shipType; 		
	}
	
	public int[][] getPositionsOnGamefield(){
		return positionsOnGamefield;
	}
	
	public String getName() {
		return name;		
	}
	
	/** 
	 * @param rowIndex
	 * @param columnIndex
	 * creates a mask of the gamefield where only this ship is visible
	 */
	
	public void setPositionsOnGamefield(int rowIndex, int columnIndex){
		positionsOnGamefield[rowIndex][columnIndex] = 1;
	}	
	
}
