package game;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class KI implements Runnable {

	private Lock _mutex = new ReentrantLock();
	private Condition con = _mutex.newCondition();	
	
	private static KI instance;
	
	private boolean terminated = false;
	
	private boolean shipHit = false;
	private boolean shipWasHitNotSunk = false;
	
	private GameField gf = GameField.getInstanceOf();	
	private int[][] opponentGamefield;
	
	private int rowIndex;
	private int columnIndex;
	
	private int variation;	
		
	private int rowIndexHit;
	private int columnIndexHit;
	
	private int firstHitIndexRow;
	private int firstHitIndexColumn;
	
	private KI() {
		opponentGamefield = gf.getPlayerGamefield();
	}
	
	public static KI getInstanceOf() {
		if(instance == null) {
			instance = new KI();
		}
		return instance;
	}
	
	public void run() {
		
		while(!terminated) {
			_mutex.lock();
			try {				
				makeDecisionForFire();
				fire();	
				System.out.println("NPC made his set: \n\n" + gf.toString());
				
				con.await();
			} catch (InterruptedException e) {				
				System.err.println(e);
			}	
			_mutex.unlock();
		}		
	}
	
	private void doVariation() {
		
		switch(variation) {
		
			case 0: 
				if (rowIndex+1 <= 12) {
					rowIndex++;					
				} else {
					incVariation();
					doVariation();
				}
				break;	
					
					
			case 1: 
				if (rowIndex-1 >= 0) {
					rowIndex--;
				} else {
					incVariation();
					doVariation();
				}
				break;
				
			case 2: 
				if (columnIndex+1 <= 11) {
					columnIndex++;
				} else {
					incVariation();
					doVariation();
				}
				break;
				
			case 3: 
				if (columnIndex-1 >= 0) {
					columnIndex--;
				} else {
					incVariation();
					doVariation();
				}				
				break;		
		}
	}
	
	private void incVariation() {
		if (variation < 3) {
			variation++;
		} else {
			variation = 0;
		}
	}
	
	private void makeDecisionForFire() {
				
		if (!shipWasHitNotSunk) {
			rowIndex = (int) (Math.random()*13);
			columnIndex = (int) (Math.random()*12);
		
		} else if (!shipHit && shipWasHitNotSunk) {
			rowIndex = firstHitIndexRow;
			columnIndex = firstHitIndexColumn;
						
			/* variation[i+1] */
			incVariation();
			doVariation();			
			
		} else if (shipHit && shipWasHitNotSunk) {					
			/* variation[i] */
			doVariation();			
		}
	}
	
	private void fire() {
		System.out.println("NPC fires at:[" + rowIndex + "] [" + columnIndex + "]!");
		if (opponentGamefield[rowIndex][columnIndex] == 1) {
			opponentGamefield[rowIndex][columnIndex] = 2;
			if (!shipWasHitNotSunk) {
				firstHitIndexRow = rowIndex;
				firstHitIndexColumn = columnIndex;
			}
			shipHit = true;
			rowIndexHit = rowIndex; 
			columnIndexHit = columnIndex;
			if(gf.whichShipIsHit(rowIndex, columnIndex, true) == 0) {      //if return 0 ship is sunk			
				shipWasHitNotSunk = false;
			} else {				
				shipWasHitNotSunk = true;
			}			
		} else {			
			shipHit = false;			
		}
	}
	
	public void signalThread() {
		_mutex.lock();
		con.signal();
		_mutex.unlock();
	}
	
	public void terminate() {
		terminated = true;
	}
}
