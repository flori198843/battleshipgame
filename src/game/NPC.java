package game;

import ships.Watercraft;

public class NPC {

	private int[][] playerGamefield;	
	//private ArrayList<Watercraft> shipList;
		
	private GameField gf = GameField.getInstanceOf();	
	private static NPC instance;	
	
	/* Constructor */
	
	private NPC() {		
		playerGamefield = gf.getNPCGamefield();		
	}
	
	public static NPC getInstanceOf() {
		if (instance == null) {
			instance = new NPC();
		}
		return instance;
	}
	
	/**  
	 * @param ship
	 * @param rowIndex
	 * @param columnIndex
	 * @param direction
	 * puts the nose of specific ship on gamefield[rowIndex][columnIndex]
	 * direction 0 for up, 1 for down, 2 for right, 3 for left.
	 */
	
	public void setShipOnGameField(Watercraft ship) {
		
		int rowIndex = (int) (Math.random()*13);
		int columnIndex = (int) (Math.random()*12);
		int direction = (int) (Math.random()*4);
		
		if (gf.setShipOnGamefieldIsLegit(playerGamefield, ship.getMaxHits(), rowIndex, columnIndex, direction)) {
			
			switch(direction) {
				case 0:
					for (int i = 0; i < ship.getMaxHits(); i++) {
						playerGamefield[rowIndex+i][columnIndex] = 1; 
						ship.setPositionsOnGamefield(rowIndex+i, columnIndex);
				}
				break;
					
				case 1:
					for (int i = 0; i < ship.getMaxHits(); i++) {
						playerGamefield[rowIndex-i][columnIndex] = 1; 
						ship.setPositionsOnGamefield(rowIndex-i, columnIndex);
				}
				break;
					
				case 2:
					for (int i = 0; i < ship.getMaxHits(); i++) {
						playerGamefield[rowIndex][columnIndex+i] = 1; 
						ship.setPositionsOnGamefield(rowIndex, columnIndex+i);
				}
				break;
				case 3:
					for (int i = 0; i < ship.getMaxHits(); i++) {
						playerGamefield[rowIndex][columnIndex-i] = 1; 
						ship.setPositionsOnGamefield(rowIndex, columnIndex-i);
				}
				break;	
			}	
		} else {
			setShipOnGameField(ship);
		}
	}	
	

}
