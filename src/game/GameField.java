package game;

import java.util.Iterator;

import ships.Watercraft;

public class GameField {

	private int[][] playerGameField;
	private int[][] npcGameField;
	private final int gameFieldCntColumns = 12;
	private final int gameFieldCntRows = 13;
	private static GameField instance;	
	
	/* Constructor */
	
	private GameField() {		
		playerGameField = new int[gameFieldCntRows][gameFieldCntColumns];
		npcGameField = new int[gameFieldCntRows][gameFieldCntColumns];
	}
	
	public static GameField getInstanceOf() {
		if(instance == null) {
			instance = new GameField();
		}
		return instance;
	}
	
	/* print out the gamefields for debug purpose */
	
	public String toString() {
		
		String out = "Gamefield of NPC: \n";
		
		for (int i = 0; i < gameFieldCntRows; i++) {
			for (int j = 0; j < gameFieldCntColumns; j++) {
				if (j%gameFieldCntColumns == 0) {
					out = out + "\n|" + npcGameField[i][j] + "|" ;					
				} else {
					out = out + "|" + npcGameField[i][j] + "|" ;
				}								
			}			
		}
		out = out + "\n\n\n Gamefield of Player: \n";
		for (int i = 0; i < gameFieldCntRows; i++) {
			for (int j = 0; j < gameFieldCntColumns; j++) {
				if (j%gameFieldCntColumns == 0) {
					out = out + "\n|" + playerGameField[i][j] + "|" ;					
				} else {
					out = out + "|" + playerGameField[i][j] + "|" ;
				}								
			}			
		}
		
		return out;
	}
	
	/**	 * 
	 * @param gamefield
	 * @param maxHits
	 * @param rowIndex
	 * @param columnIndex
	 * @param direction
	 * @return true, if no intersections are made by placing a ship
	 */
	
	public boolean setShipOnGamefieldIsLegit(int[][] gamefield, int maxHits, int rowIndex, int columnIndex, int direction) {
		boolean isLegit = true;
		
		switch(direction) {
		case 0:
			for (int i = 0; i < maxHits; i++) {
				if(rowIndex+i > gameFieldCntRows - 1){					
					isLegit = false;
					break;
				} else if (gamefield[rowIndex+i][columnIndex] == 1) {
					isLegit = false;
					i = maxHits;
				}					
		}
		break;
			
		case 1:
			for (int i = 0; i < maxHits; i++) {
				if (rowIndex-i < 0 ) {					
					isLegit = false;
					break;
				} else if (gamefield[rowIndex-i][columnIndex] == 1) {
					isLegit = false;
					i = maxHits;
				}						
		}
		break;
			
		case 2:
			for (int i = 0; i < maxHits; i++) {
				if (columnIndex+i > gameFieldCntColumns - 1) {					
					isLegit = false;
					break;
				} else if (gamefield[rowIndex][columnIndex+i] == 1) {
					isLegit = false;
					i = maxHits;
				}				
		}
		break;
		case 3:
			for (int i = 0; i < maxHits; i++) {
				if (columnIndex-i < 0) {					
					isLegit = false;
					break;
				} else if	(gamefield[rowIndex][columnIndex-i] == 1 ) {
					isLegit = false;
					i = maxHits;
				}			
		}
		break;	
	}		
		
		return isLegit;				
	}
	
	public int whichShipIsHit(int rowIndex, int columnIndex, boolean player) {
		String whichPlayer = "";
		int sunk = -1;
		Iterator<Watercraft> iter;  
		if(!player) {
			iter = Watercraft.shipsNPC.iterator();
			whichPlayer = "NPC";
		} else {
			iter = Watercraft.shipsPlayer.iterator();
			whichPlayer = "Player";
		}
						
		while (iter.hasNext()) {
			Watercraft ship = iter.next();			
			if (ship.getPositionsOnGamefield()[rowIndex][columnIndex] == 1) {
				ship.decMaxHits();
				if (ship.getMaxHits() == 0) {
					ship.setDestroyed(iter);	
					sunk = 0;
					System.out.println(ship.getName() + " of " + whichPlayer + " was hit and completely destroyed!");
				} else {					
					System.out.println(ship.getName() + " of " + whichPlayer + " was hit!");					
				}				
			}
		}
		return sunk;
	}
	
	/* Getter and Setter */
	
	public int[][] getPlayerGamefield(){
		return playerGameField;
	}
	
	public int[][] getNPCGamefield(){
		return npcGameField;
	}
	
}
