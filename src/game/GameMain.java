package game;

import java.util.Scanner;

import ships.Aircraftcarrier;
import ships.Battleship;
import ships.Cruiser;
import ships.Destroyer;
import ships.Watercraft;

public class GameMain {

	private GameField gf = GameField.getInstanceOf();
	
	/* NPC ships */
	private Destroyer destroyer1 = new Destroyer();
	private Destroyer destroyer2 = new Destroyer();	
	private Destroyer destroyer3 = new Destroyer();	
	private Destroyer destroyer4 = new Destroyer();	
	
	private Cruiser cruiser1 = new Cruiser();
	private Cruiser cruiser2 = new Cruiser();
	private Cruiser cruiser3 = new Cruiser();
	
	private Battleship battleship1 = new Battleship();
	private Battleship battleship2 = new Battleship();
	
	private Aircraftcarrier aricraftcarrier = new Aircraftcarrier();
	
	/* Player ships */
	private Destroyer destroyer5 = new Destroyer();
	private Destroyer destroyer6 = new Destroyer();	
	private Destroyer destroyer7 = new Destroyer();	
	private Destroyer destroyer8 = new Destroyer();	
	
	private Cruiser cruiser4 = new Cruiser();
	private Cruiser cruiser5 = new Cruiser();
	private Cruiser cruiser6 = new Cruiser();
	
	private Battleship battleship3 = new Battleship();
	private Battleship battleship4 = new Battleship();
	
	private Aircraftcarrier aricraftcarrier2 = new Aircraftcarrier();
	
	private Player p1 = new Player("Florian");
	NPC npc = NPC.getInstanceOf();
	KI ki = KI.getInstanceOf();
	
	public GameMain() {
		
		setShipsIntoLists();
		setNPCShipsOnGamefield();
		setPlayerShipsOnGamefield();
	}
	
	private void setNPCShipsOnGamefield() {	
			npc.setShipOnGameField(destroyer1);		
			npc.setShipOnGameField(destroyer2);	
			npc.setShipOnGameField(destroyer3);	
			npc.setShipOnGameField(destroyer4);	
			
			npc.setShipOnGameField(cruiser1);	
			npc.setShipOnGameField(cruiser2);	
			npc.setShipOnGameField(cruiser3);	
			
			npc.setShipOnGameField(battleship1);	
			npc.setShipOnGameField(battleship2);	
			
			npc.setShipOnGameField(aricraftcarrier);					
	}
	
	/* direction 0 for up, 1 for down, 2 for right, 3 for left */
	private void setPlayerShipsOnGamefield() {	
		p1.setShipOnGameField(destroyer5);		
		p1.setShipOnGameField(destroyer6);	
		p1.setShipOnGameField(destroyer7);	
		p1.setShipOnGameField(destroyer8);	
		
		p1.setShipOnGameField(cruiser4);	
		p1.setShipOnGameField(cruiser5);	
		p1.setShipOnGameField(cruiser6);	
		
		p1.setShipOnGameField(battleship3);	
		p1.setShipOnGameField(battleship4);	
		
		p1.setShipOnGameField(aricraftcarrier2);					
}
	
	private void setShipsIntoLists() {
		/* NPC Ships */
		destroyer1.addShipToLists(false);	
		destroyer2.addShipToLists(false);
		destroyer3.addShipToLists(false);
		destroyer4.addShipToLists(false);
		
		cruiser1.addShipToLists(false);
		cruiser2.addShipToLists(false);
		cruiser3.addShipToLists(false);
		
		battleship1.addShipToLists(false);
		battleship2.addShipToLists(false);
		
		aricraftcarrier.addShipToLists(false);	
		
		/* Player Ships */
		destroyer5.addShipToLists(true);	
		destroyer6.addShipToLists(true);
		destroyer7.addShipToLists(true);
		destroyer8.addShipToLists(true);
		
		cruiser4.addShipToLists(true);
		cruiser5.addShipToLists(true);
		cruiser6.addShipToLists(true);
		
		battleship3.addShipToLists(true);
		battleship4.addShipToLists(true);
		
		aricraftcarrier2.addShipToLists(true);	
	}
	
	public void runGameInConsole() {		
		
		//Scanner scanInput = new Scanner(System.in);
		
		int rowIndex;
		int columnIndex;
		
		int cntRounds = 1;
		
		System.out.println(gf.toString());
		
		Thread kiThread = new Thread(ki);
		kiThread.start();
				
		while (!Watercraft.shipsNPC.isEmpty() && !Watercraft.shipsPlayer.isEmpty()) {			
			
//			System.out.println("Bitte Zeile eingeben: ");
//			rowIndex = Integer.parseInt(scanInput.next())-1;
//			System.out.println("Bitte Spalte eingeben: ");
//			columnIndex = Integer.parseInt(scanInput.next())-1;
			
			p1.fire(1, 1);
			
			System.out.println(gf.toString());
			
			ki.signalThread();
			cntRounds++;
		}		
		ki.terminate();
		System.out.println("\nGame finished after " + cntRounds + " rounds!");
	}
	
	public static void main(String[] args) {				
		
		GameMain gameMain = new GameMain();
		
		gameMain.runGameInConsole();		
	}
}
