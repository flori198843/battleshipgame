package game;

import ships.Aircraftcarrier;
import ships.Battleship;
import ships.Cruiser;
import ships.Destroyer;
import ships.Watercraft;

public class Player {

	private String name;
	private int playerID;
	private static int playerIDCnt = 1;	
	private int[][] playerGamefield;
	private int[][] opponentGamefield;
	private NPC npc = NPC.getInstanceOf();
	
	private GameField gf = GameField.getInstanceOf();	
	
	/* Constructor */
	
	public Player(String name) {		
		this.name = name;
		playerID = playerIDCnt++;	
		playerGamefield = gf.getPlayerGamefield();
		opponentGamefield = gf.getNPCGamefield();
	}
	
	/**  
	 * @param ship
	 * @param rowIndex
	 * @param columnIndex
	 * @param direction
	 * puts the nose of specific ship on gamefield[rowIndex][columnIndex]
	 * direction 0 for up, 1 for down, 2 for right, 3 for left.
	 */
	
//	public void setShipOnGameField(Watercraft ship, int rowIndex, int columnIndex, int direction) {
//		
//		if (gf.setShipOnGamefieldIsLegit(playerGamefield, ship.getMaxHits(), rowIndex, columnIndex, direction)) {
//			
//			switch(direction) {
//				case 0:
//					for (int i = 0; i < ship.getMaxHits(); i++) {
//						playerGamefield[rowIndex+i][columnIndex] = 1; 						
//				}
//				break;
//					
//				case 1:
//					for (int i = 0; i < ship.getMaxHits(); i++) {
//						playerGamefield[rowIndex-i][columnIndex] = 1; 
//				}
//				break;
//					
//				case 2:
//					for (int i = 0; i < ship.getMaxHits(); i++) {
//						playerGamefield[rowIndex][columnIndex+i] = 1; 
//				}
//				break;
//				case 3:
//					for (int i = 0; i < ship.getMaxHits(); i++) {
//						playerGamefield[rowIndex][columnIndex-i] = 1; 
//				}
//				break;	
//			}	
//		} else {
//			System.err.println("ERROR: Please place ship without any intersections!");
//		}
//	}	
	
	public void setShipOnGameField(Watercraft ship) {
		
		int rowIndex = (int) (Math.random()*13);
		int columnIndex = (int) (Math.random()*12);
		int direction = (int) (Math.random()*4);
		
		if (gf.setShipOnGamefieldIsLegit(playerGamefield, ship.getMaxHits(), rowIndex, columnIndex, direction)) {
			
			switch(direction) {
				case 0:
					for (int i = 0; i < ship.getMaxHits(); i++) {
						playerGamefield[rowIndex+i][columnIndex] = 1; 
						ship.setPositionsOnGamefield(rowIndex+i, columnIndex);
				}
				break;
					
				case 1:
					for (int i = 0; i < ship.getMaxHits(); i++) {
						playerGamefield[rowIndex-i][columnIndex] = 1; 
						ship.setPositionsOnGamefield(rowIndex-i, columnIndex);
				}
				break;
					
				case 2:
					for (int i = 0; i < ship.getMaxHits(); i++) {
						playerGamefield[rowIndex][columnIndex+i] = 1; 
						ship.setPositionsOnGamefield(rowIndex, columnIndex+i);
				}
				break;
				case 3:
					for (int i = 0; i < ship.getMaxHits(); i++) {
						playerGamefield[rowIndex][columnIndex-i] = 1; 
						ship.setPositionsOnGamefield(rowIndex, columnIndex-i);
				}
				break;	
			}	
		} else {
			setShipOnGameField(ship);
		}
	}
	
	public void fire(int rowIndex, int columnIndex) {
		if (opponentGamefield[rowIndex][columnIndex] == 1) {
			opponentGamefield[rowIndex][columnIndex] = 2;
			gf.whichShipIsHit(rowIndex, columnIndex, false);
		}
	}	
	
	/* Getters and Setters */
	
	public String getName() {
		return name;
	}
	
	public int getPlayerID() {
		return playerID;
	}	
	
}
