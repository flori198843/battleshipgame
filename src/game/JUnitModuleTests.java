package game;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

//import ships.Aircraftcarrier;
//import ships.Battleship;
import ships.Destroyer;
import ships.Watercraft;

class JUnitModuleTests {	
	@Test
	void methodsOfClassShipTesting() {
				
		//Aircraftcarrier aircraftcarrier = new Aircraftcarrier();
		
		//Battleship battleship = new Battleship();
		
		Destroyer destroyer = new Destroyer();
		
//		destroyer.addShipToLists(1);
			
		/* Look for the ship being in both lists! */
		
		assertEquals(1, Watercraft.shipsNPC.size());
		assertEquals(1, Watercraft.shipsPlayer.size());
		
//		destroyer.setDestroyed(Watercraft.shipsNPC);
//		destroyer.setDestroyed(Watercraft.shipsPlayer);
		
		/* Look for the ship being removed out of both lists! */
		
		assertEquals(0, Watercraft.shipsNPC.size());
		assertEquals(0, Watercraft.shipsPlayer.size());	
		
		/* Testing Getter and Setter */
		
		assertEquals(2, destroyer.getMaxHits());	
		
		/* Decrease maxHits of ship by one */
		
		destroyer.decMaxHits();
		assertEquals(1, destroyer.getMaxHits());
		
	}
	
	@Test
	void methodsOfClassPlayerTesting() {	
		
	
		
//		assertEquals(1, p1.getPlayerID());
//		assertEquals(2, p2.getPlayerID());
	}
	
//	@Test
//	void methodsOfClassGameFieldTesting() {
//		GameField gf = new GameField();
//		System.out.println(gf.toString());
//	}
}
